all: col row


col: build_col
	./run_col $(INPUT) > test/col_$(INPUT)
	./run_col $(INPUT) >> test/col_$(INPUT)
	./run_col $(INPUT) >> test/col_$(INPUT)
	echo "COL ORDER $(INPUT)"
	cat test/col_$(INPUT)

build_col: compile main_col.c matrix.o
	gcc -o run_col main_col.c matrix.o -lm

row: build_row
	./run_row $(INPUT) > test/row_$(INPUT)
	./run_row $(INPUT) >> test/row_$(INPUT)
	./run_row $(INPUT) >> test/row_$(INPUT)
	echo "ROW ORDER $(INPUT)"
	cat test/row_$(INPUT)

build_row: compile main_row.c matrix.o
	gcc -o run_row main_row.c matrix.o -lm

compile: matrix.c matrix.h
	gcc -c matrix.c

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "matrix.h"

double getElapsedTime(struct timespec, struct timespec);


int main(int argc, char **argv) {
  Matrix X;
  Vector Y, r, xy, x2, x;
  int MATRIX_SIZE, i, j;
  struct timespec start, end;

  MATRIX_SIZE = atoi(argv[1]);
  X = createMatrix(MATRIX_SIZE, MATRIX_SIZE);
  Y = createVector(MATRIX_SIZE);

  populateMatrix(X, MATRIX_SIZE, MATRIX_SIZE);
  populateVector(Y, MATRIX_SIZE);

  clock_gettime(CLOCK_REALTIME, &start);
  r = pearsonCor_col(X, Y, MATRIX_SIZE, MATRIX_SIZE);
  clock_gettime(CLOCK_REALTIME, &end);

  printf("time elapsed: %fs\n", getElapsedTime(start, end));

  return 0;
}

// techiedelight.com/find-execution-time-c-program for the system time calculation
double getElapsedTime(struct timespec start, struct timespec end) {
  return (end.tv_sec - start.tv_sec)+(end.tv_nsec - start.tv_nsec)/1000000000.0;
}

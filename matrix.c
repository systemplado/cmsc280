#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include "matrix.h"


Matrix createMatrix(int m, int n) {
  int i, j;

  Matrix X = malloc(sizeof(Vector) * m);

  for (i=0; i<m; i++) {
    X[i] = createVector(n);
  }

  return X;
}

Vector createVector(int n) {
  int i;

  Vector y = malloc(sizeof(int) *n);
  return y;
}

void populateMatrix(Matrix X, int m, int n) {
  int i, j;

  srand(time(NULL));

  for (i=0; i<m; i++) {
    for (j=0; j<m; j++) {
      X[i][j] = rand() % 100;
    }
  }
}

void populateVector(Vector X, int m) {
  int i;

  for (i=0; i<m; i++) {
    X[i] = rand() % 100;
  }
}

Matrix freeMatrix(Matrix X, int m) {
  int i;

  for (i=0; i<m; i++) {
    X[i] = freeVector(X[i]);
  }

  free(X);
  X = NULL;
  return NULL;
}

Vector freeVector(Vector X) {
  free(X);
  X = NULL;

  return NULL;
}

Vector pearsonCor_row(Matrix X, Vector Y, int m, int n) {
  int i, j;
  float y2 = 0, y = 0, k;
  Vector r, xy, x2, x;

  r = createVector(n);
  x = createVector(n);
  xy = createVector(n);
  x2 = createVector(n);

  for (i=0; i<m; i++) {
    r[i] = 0;
    x[i] = 0;
    xy[i] = 0;
    x2[i] = 0;

    y += Y[i];
    y2 += Y[i] * Y[i];
  }

  k = m*y2 - y*y;

  for (i=0; i<m; i++) {
    for (j=0; j<n; j++) {
      x[j] += X[i][j];
      x2[j] += X[i][j] * X[i][j];
      xy[j] += X[i][j] * Y[i];
    }

    r[i] = (m * xy[i] - x[i]*y) /
      ( sqrt( (m*x2[i] - x[i]*x[i]) * k ) );
  }

  freeVector(x);
  freeVector(xy);
  freeVector(x2);

  return r;
}

Vector pearsonCor_col(Matrix X, Vector Y, int m, int n) {
  int i, j;
  float y2 = 0, y = 0, k;
  Vector r, xy, x2, x;

  r = createVector(n);
  x = createVector(n);
  xy = createVector(n);
  x2 = createVector(n);

  for (i=0; i<m; i++) {
    r[i] = 0;
    x[i] = 0;
    xy[i] = 0;
    x2[i] = 0;

    y += Y[i];
    y2 += Y[i] * Y[i];
  }

  k = m*y2 - y*y;

  for (i=0; i<m; i++) {
    for (j=0; j<n; j++) {
      x[i] += X[j][i];
      x2[i] += X[j][i] * X[j][i];
      xy[i] += X[j][i] * Y[j];
    }

    r[i] = (m * xy[i] - x[i]*y) /
      ( sqrt( (m*x2[i] - x[i]*x[i]) * k ) );
  }

  freeVector(x);
  freeVector(xy);
  freeVector(x2);

  return r;
}

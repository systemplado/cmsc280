typedef float* Vector;
typedef Vector* Matrix;


Matrix createMatrix(int, int);
Vector createVector(int);
void populateMatrix(Matrix, int, int);
void populateVector(Vector, int);
Matrix freeMatrix(Matrix, int);
Vector freeVector(Vector);
Vector pearsonCor_row(Matrix, Vector, int, int);
Vector pearsonCor_col(Matrix, Vector, int, int);
